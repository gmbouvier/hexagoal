#!/usr/bin/python

from graphics import *
import math
import time
import random

def rad(deg):
    return math.pi / 180 * (60 * deg - 30)

def evenr_to_cube(col, row):
    x = col - (row + (row % 2)) / 2
    return (x, row, -x-row)

class Cell:
    def __init__(self, polygon, alive):
        self.polygon = polygon
        if alive:
            self.live()
        else:
            self.die()

    def live(self):
        self.alive = True
        self.polygon.setFill("black")

    def die(self):
        self.alive = False
        self.polygon.setFill("grey")

    def life(self, neighbors):
        if neighbors < 3 and self.alive:
            self.die()
        elif (neighbors > 2 and neighbors < 5):
            self.live()
        elif self.alive:
            self.die()

class Window:
    def __init__(self, window_width, window_height, grid_size):
        self.window_height = window_height
        self.window_width = window_width
        self.grid_size = grid_size
        self.cell_width = math.sqrt(3) * self.grid_size
        self.cell_height = (2 * self.grid_size) * 0.75
        self.center = Point(window_width/2, window_height/2)
        self.grid_height = int(self.window_height/self.cell_height) + 4
        self.grid_width = int(self.window_width/self.cell_width) + 4
        self.cells = {}
        self.cube_cells = {}
        self.window = GraphWin("window",
                               window_width,
                               window_height,
                               autoflush=False)

    def clear(self):
        [self.cells[v].die() for v in self.cells]

    def random(self):
        [
            cell.live()
            if random.choice([True, False])
            else cell.die()
            for cell in self.cells.values()
        ]

    def hexagon(self, position):
        points = [
            Point(position.x + self.grid_size * math.cos(rad(n)),
                  position.y + self.grid_size * math.sin(rad(n)))
            for n in range(6)
        ]
        return Polygon(points)

    def draw_grid(self):
        for i in range(int(-(self.grid_height/2)), int(self.grid_height/2)):
            for j in range(int(-(self.grid_width/2)), int(self.grid_width/2)):
                if not self.cells.get((j, i), False):
                    self.cells[(j, i)] = Cell(self.hexagon(self.grid_to_pos(j,i)), False)
                cell = self.cells.get((j, i))
                self.cube_cells[evenr_to_cube(j, i)] = cell
                cell.polygon.draw(self.window)

    def grid_to_pos(self, x, y):
        offset = self.cell_width / 2 if y % 2 != 0 else 0
        return Point(x * self.cell_width - offset + (self.window_width/2), y * self.cell_height + (self.window_height/2))

    def neighbors(self, cube):
        x, y, z = cube
        li = []
        for n in [-1, 1]:
            li.append((x+n, y-n, z))
            li.append((x, y+n, z-n))
            li.append((x-n, y, z+n))
        return sum(self.cube_cells[cube].alive for cube in li if self.cube_cells.get(cube, False))

    def turn(self):
        for cube, cell in self.cube_cells.items():
            cell.life(self.neighbors(cube))

win = Window(1920, 1080, 10)

win.draw_grid()
win.random()

for i in range(1000):
    update(5)
    win.turn()
    if len([cell for cell in win.cells.values() if cell.alive == True]) == 0:
        break
